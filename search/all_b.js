var searchData=
[
  ['id_5f',['id_',['../db/d5b/classcotask_1_1task.html#ab2974f421103cbb28d83f8d324dba728',1,'cotask::task']]],
  ['id_5fallocator',['id_allocator',['../dd/d52/classcotask_1_1impl_1_1id__allocator.html',1,'cotask::impl']]],
  ['id_5fallocator_5fimpl_2eh',['id_allocator_impl.h',['../d7/d74/id__allocator__impl_8h.html',1,'']]],
  ['id_5fallocator_5ft',['id_allocator_t',['../db/d5b/classcotask_1_1task.html#acb04d7cc99f58c4e15bf4e05bd8f8d07',1,'cotask::task::id_allocator_t()'],['../dd/d93/structcotask_1_1macro__task.html#ac6ac097519256397079bd08af900e700',1,'cotask::macro_task::id_allocator_t()']]],
  ['id_5fallocator_5ftest_2ecpp',['id_allocator_test.cpp',['../de/d33/id__allocator__test_8cpp.html',1,'']]],
  ['id_5ft',['id_t',['../db/d5b/classcotask_1_1task.html#abbb1b757382c7db3bf463f7cbef9b1a5',1,'cotask::task::id_t()'],['../dd/d93/structcotask_1_1macro__task.html#ab382aaa1f05f0863fddff3f2c1045102',1,'cotask::macro_task::id_t()'],['../d6/ddc/classcotask_1_1task__manager.html#a26e8954faf0609d14b687c9811e67d3b',1,'cotask::task_manager::id_t()']]],
  ['inc',['inc',['../d8/d3d/structutil_1_1lock_1_1detail_1_1atomic__msvc__oprs_3_011_01_4.html#ac0686096d82420f529d3026b290f55ed',1,'util::lock::detail::atomic_msvc_oprs&lt; 1 &gt;::inc()'],['../d1/d19/structutil_1_1lock_1_1detail_1_1atomic__msvc__oprs_3_012_01_4.html#a7aca6e20040f04fd213399bad6610b92',1,'util::lock::detail::atomic_msvc_oprs&lt; 2 &gt;::inc()'],['../da/dbf/structutil_1_1lock_1_1detail_1_1atomic__msvc__oprs_3_018_01_4.html#a5d9825744ad3a8bd632f7febca0a6d51',1,'util::lock::detail::atomic_msvc_oprs&lt; 8 &gt;::inc()'],['../d1/dc9/structutil_1_1lock_1_1detail_1_1atomic__msvc__oprs.html#ae1726b41dd164561bd89feca7c8e2f3f',1,'util::lock::detail::atomic_msvc_oprs::inc()']]],
  ['init_5fkey_5fvalue_5fmap',['init_key_value_map',['../d1/df6/classutil_1_1cli_1_1cmd__option__list.html#a646d13ee7e86dbc2f147c23b473320dc',1,'util::cli::cmd_option_list']]],
  ['init_5fpthread_5fthis_5fcoroutine_5fcontext',['init_pthread_this_coroutine_context',['../de/d11/namespacecopp_1_1detail.html#a3ea6ccd7bcf7df1339bb850c195f88a7',1,'copp::detail']]],
  ['insert',['insert',['../df/d63/namespaceutil_1_1cli_1_1phoenix.html#a8a1d7c5c5efe2c1a5bec72fa850202e0',1,'util::cli::phoenix']]],
  ['insert_5ft',['insert_t',['../de/df6/structutil_1_1cli_1_1phoenix_1_1insert__t.html',1,'util::cli::phoenix']]],
  ['insert_5ft',['insert_t',['../de/df6/structutil_1_1cli_1_1phoenix_1_1insert__t.html#a2bc866e80697ad378b423fc81f70b5c3',1,'util::cli::phoenix::insert_t']]],
  ['instacne_5f',['instacne_',['../dd/dc1/classcotask_1_1task__action__mem__function.html#a8d580d683e83295c49d14a4c5f5b8d54',1,'cotask::task_action_mem_function::instacne_()'],['../d5/d9d/classcotask_1_1task__action__mem__function_3_01int_00_01Tc_01_4.html#a894685265083fc906eb356b22ca451e5',1,'cotask::task_action_mem_function&lt; int, Tc &gt;::instacne_()']]],
  ['intrusive_5fptr',['intrusive_ptr',['../d6/d8f/classstd_1_1intrusive__ptr.html',1,'std']]],
  ['intrusive_5fptr',['intrusive_ptr',['../d6/d8f/classstd_1_1intrusive__ptr.html#ae0004efc13971afea5b6a3e1b4b58874',1,'std::intrusive_ptr::intrusive_ptr()'],['../d6/d8f/classstd_1_1intrusive__ptr.html#a8a7abc556e8220ba1e329abaa3acdfdf',1,'std::intrusive_ptr::intrusive_ptr() UTIL_CONFIG_NOEXCEPT'],['../d6/d8f/classstd_1_1intrusive__ptr.html#ad51af3f0cc790fbd015422bbcdb0091b',1,'std::intrusive_ptr::intrusive_ptr(T *p, bool add_ref=true)'],['../d6/d8f/classstd_1_1intrusive__ptr.html#a5ebf5252461e87e620afc9f50d9471d9',1,'std::intrusive_ptr::intrusive_ptr(intrusive_ptr&lt; U &gt; const &amp;rhs)'],['../d6/d8f/classstd_1_1intrusive__ptr.html#a901b39054207606db8518633ed6c83fe',1,'std::intrusive_ptr::intrusive_ptr(self_type const &amp;rhs)']]],
  ['intrusive_5fptr_2eh',['intrusive_ptr.h',['../db/d02/intrusive__ptr_8h.html',1,'']]],
  ['intrusive_5fptr_5fadd_5fref',['intrusive_ptr_add_ref',['../d0/d14/classcopp_1_1coroutine__context__container.html#a68a1e61c81ced6a7e245dcaa1cdacc0b',1,'copp::coroutine_context_container::intrusive_ptr_add_ref()'],['../db/d5b/classcotask_1_1task.html#a6aaa24a42d41d24979e1e746137c1bb9',1,'cotask::task::intrusive_ptr_add_ref()']]],
  ['intrusive_5fptr_5frelease',['intrusive_ptr_release',['../d0/d14/classcopp_1_1coroutine__context__container.html#ae66803423f720b86af90bc1bd5e1776e',1,'copp::coroutine_context_container::intrusive_ptr_release()'],['../db/d5b/classcotask_1_1task.html#a93f5fcfe02f54bd1a960b6992b8b73a0',1,'cotask::task::intrusive_ptr_release()']]],
  ['is_5fauto_5fgc',['is_auto_gc',['../d1/d78/classcopp_1_1stack__pool.html#a5f2c0bef5cbac06daa37caf2471c0f66',1,'copp::stack_pool']]],
  ['is_5favailable',['is_available',['../d7/de5/classutil_1_1lock_1_1lock__holder.html#ae2a4960b08216476a4c29397d6871783',1,'util::lock::lock_holder']]],
  ['is_5fcanceled',['is_canceled',['../d1/d7a/classcotask_1_1impl_1_1task__impl.html#a07771b64c29544ab6f2c20a835685ecc',1,'cotask::impl::task_impl']]],
  ['is_5fcompleted',['is_completed',['../d1/d7a/classcotask_1_1impl_1_1task__impl.html#a4ce3f62ab1fd52785afe1e57b55d6885',1,'cotask::impl::task_impl::is_completed()'],['../db/d5b/classcotask_1_1task.html#a73ee8dea7c6b0f03ec917abc3ea87069',1,'cotask::task::is_completed()']]],
  ['is_5fexiting',['is_exiting',['../d1/d7a/classcotask_1_1impl_1_1task__impl.html#ac45a52ddf7b25812684a1345db0366cd',1,'cotask::impl::task_impl']]],
  ['is_5ffaulted',['is_faulted',['../d1/d7a/classcotask_1_1impl_1_1task__impl.html#a35fb072ea9a64c32d258ebe8165afe06',1,'cotask::impl::task_impl']]],
  ['is_5ffinished',['is_finished',['../db/dbb/classcopp_1_1coroutine__context.html#a59d02a998eb9de15bb041023297d9fa3',1,'copp::coroutine_context']]],
  ['is_5flocked',['is_locked',['../da/db3/classutil_1_1lock_1_1spin__lock.html#a4ecd86f4e1c27e7ef06dbc088e0957b5',1,'util::lock::spin_lock']]],
  ['is_5ftimeout',['is_timeout',['../d1/d7a/classcotask_1_1impl_1_1task__impl.html#a943bae6a120fc13e51080b1ae8399d57',1,'cotask::impl::task_impl']]],
  ['is_5funbounded',['is_unbounded',['../db/d34/structcopp_1_1stack__traits.html#a643d9b647985548b9a2efaa4394c1712',1,'copp::stack_traits']]],
  ['install',['INSTALL',['../d4/dbe/md_md_010_INSTALL.html',1,'']]]
];

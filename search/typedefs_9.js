var searchData=
[
  ['param_5ftype',['param_type',['../db/d64/classutil_1_1cli_1_1binder_1_1cmd__option__bind__base.html#afb3b6c028920dcd9ac832871ed79c713',1,'util::cli::binder::cmd_option_bind_base']]],
  ['placement_5fdestroy_5ffn_5ft',['placement_destroy_fn_t',['../dc/d04/namespacecotask.html#a90701bc24130f55672855ca0397f78d3',1,'cotask']]],
  ['pool_5ft',['pool_t',['../d9/d03/classcopp_1_1allocator_1_1stack__allocator__pool.html#a2a0714d647a4c16c3cb47d087117b99c',1,'copp::allocator::stack_allocator_pool']]],
  ['ptr_5ft',['ptr_t',['../db/dbb/classcopp_1_1coroutine__context.html#a195084f19ee63cab77e4ae232e651bf4',1,'copp::coroutine_context::ptr_t()'],['../d0/d14/classcopp_1_1coroutine__context__container.html#a1ec7f489ab400c929b290eb5dbc376b4',1,'copp::coroutine_context_container::ptr_t()'],['../d1/d78/classcopp_1_1stack__pool.html#a06b1b3ccddfe6077e323f2902daae2c6',1,'copp::stack_pool::ptr_t()'],['../db/d5b/classcotask_1_1task.html#ac53375e92cd090fa905ebcee8d61f7ac',1,'cotask::task::ptr_t()'],['../d6/ddc/classcotask_1_1task__manager.html#ababd56c114149258644527406c532fe2',1,'cotask::task_manager::ptr_t()']]],
  ['ptr_5ftype',['ptr_type',['../d1/d9e/classutil_1_1cli_1_1cmd__option__bind.html#a4333f60a71e7237920773c8bc1ab951a',1,'util::cli::cmd_option_bind']]]
];
